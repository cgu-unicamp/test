package br.unicamp.cgu.camunda.service;

import java.util.Map;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
public class SigadService implements JavaDelegate {
	private static final Logger LOGGER = LoggerFactory.getLogger(SigadService.class);
	
	@Override
	public void execute(DelegateExecution de) throws Exception {
		
		Map<String, Object> variables = de.getVariables();
		
		LOGGER.info("Chamando SIGAD: " + de.getCurrentActivityId());
	}

}