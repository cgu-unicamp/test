package br.unicamp.cgu.camunda.service;

import java.util.List;
import java.util.Map;

import br.unicamp.cgu.camunda.exception.DadosInvalidosException;
import br.unicamp.cgu.camunda.exception.TarefaNaoEncontradaException;
import br.unicamp.cgu.camunda.exception.TarefaNaoUnicaException;

public interface BpmService {
	
	public void iniciarProcesso(String nomeProcesso, Map<String, Object> processo) throws DadosInvalidosException;
	public void assumirTarefaPorIdProcesso(Long idProcesso, String usuario, String grupo) throws TarefaNaoEncontradaException, TarefaNaoUnicaException;
	public String alterarProcessoPorIdProcesso(Long idProcesso, Map<String, Object> processo, String usuario) throws TarefaNaoEncontradaException, TarefaNaoUnicaException, DadosInvalidosException;
	public void completarTarefaPorIdProcesso(Long idProcesso, String usuario) throws TarefaNaoEncontradaException, TarefaNaoUnicaException;
	public Map<String, Object> obterTarefaPorIdProcesso(Long idProcesso) throws TarefaNaoEncontradaException, TarefaNaoUnicaException;
	public void forcarTarefa(String tarefaAnterior, String novaTarefa, Map<String, Object> processo);
	
	public List<Map<String, Object>> listarTodasTarefas();
	public List<Map<String, Object>> listarTarefasPorDono(String dono);
	public List<Map<String, Object>> listarTarefasPorGrupo(String grupo);
	public List<Map<String, Object>> listarTarefasPorAtividade(String atividade);
	
	public List<Map<String, Object>> listarHistoricoProcesso();
	
}
