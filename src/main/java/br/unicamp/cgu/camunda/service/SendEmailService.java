package br.unicamp.cgu.camunda.service;

import java.util.Map;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Currently just a stub that "sends" the email to logger.
 * 
 * @author brouwerto
 *
 */
@Named
public class SendEmailService implements JavaDelegate {
	private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailService.class);
	
	@Override
	public void execute(DelegateExecution de) throws Exception {
		
		Map<String, Object> variables = de.getVariables();
		
		LOGGER.info("Sending email: <subject>{}<\\subject>, <body>{}<\\body>", variables.get("titulo"), variables.get("descricao"));
	}

}