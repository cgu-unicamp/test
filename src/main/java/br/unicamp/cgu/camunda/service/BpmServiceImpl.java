package br.unicamp.cgu.camunda.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricVariableInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.IdentityLink;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;

import br.unicamp.cgu.camunda.exception.DadosInvalidosException;
import br.unicamp.cgu.camunda.exception.TarefaNaoEncontradaException;
import br.unicamp.cgu.camunda.exception.TarefaNaoUnicaException;
import br.unicamp.cgu.camunda.exception.UsuarioException;
import br.unicamp.cgu.camunda.util.JsontUtil;

@Named
@Profile("production")
public class BpmServiceImpl implements BpmService {
	private RuntimeService runtimeService;
	private TaskService taskService;
	private HistoryService historyService;

	@Inject
	public BpmServiceImpl(RuntimeService runtimeService, TaskService taskService, HistoryService historyService) {
		this.runtimeService = runtimeService;
		this.taskService = taskService;
		this.historyService = historyService;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(BpmServiceImpl.class);

	@Override
	public void iniciarProcesso(String nomeProcesso, Map<String, Object> processo) throws DadosInvalidosException {

		if (processo == null || processo.get("idProcesso") == null) {
			throw new DadosInvalidosException("É necessário informar o código do processo");
		}
		
		//Qualquer um pode iniciar o processo, mas para assumir e seguir em frente é necessario estar no grupo correto.
		LOGGER.debug("Iniciando Processo: {}", JsontUtil.convertObjectToJsonString(processo));
		
		runtimeService.startProcessInstanceByKey(nomeProcesso, processo);

	}
	
	@Override
	public void assumirTarefaPorIdProcesso(Long idProcesso, String usuario, String grupo) throws TarefaNaoEncontradaException, TarefaNaoUnicaException {
		
		Map<String, Object> tarefa = obterTarefaPorIdProcesso(idProcesso);
		String taskId = (String) tarefa.get("idTarefa");
		String dono = (String) tarefa.get("dono");
		
		if (usuario == null || usuario.isEmpty()) {
			throw new UsuarioException("Usuário não informado");
		}else if (grupo == null || grupo.isEmpty()) {
			throw new UsuarioException("Grupo não informado");
		}else if (dono != null && !dono.isEmpty()) {
			throw new UsuarioException("A tarefa já foi assumida por outro usuario");
		}else {
			List<IdentityLink> links = taskService.getIdentityLinksForTask(taskId);
			
			if (links != null && !links.isEmpty() && links.get(0).getGroupId() != null) {
				if (!links.get(0).getGroupId().toLowerCase().equals(grupo.toLowerCase())) {
					throw new UsuarioException("Usuário não pertence ao grupo candidato: "+ links.get(0).getGroupId());
				}
			}
						
		}
				
		taskService.setAssignee(taskId, usuario);
		
	}

	@Override
	public void completarTarefaPorIdProcesso(Long idProcesso, String usuario) throws TarefaNaoEncontradaException, TarefaNaoUnicaException {
		
		Map<String, Object> tarefa = obterTarefaPorIdProcesso(idProcesso);
		String taskId = (String) tarefa.get("idTarefa");
		String dono = (String) tarefa.get("dono");
		String atividade = (String) tarefa.get("idAtividade"); 
		
		if (usuario == null || usuario.isEmpty()) {
			throw new UsuarioException("Usuário não informado");
		}else if (dono == null || dono.isEmpty()) {
			throw new UsuarioException("A tarefa precisa ser assumida antes de ser completada");
		}else if (!usuario.toLowerCase().equals(dono.toLowerCase())) {
			throw new UsuarioException("Usuário não é dono do processo");
		}
		
		LOGGER.debug("Completando Tarefa: {} {} por {}", taskId, atividade, usuario);
		
		taskService.complete(taskId);
	}
	
	

	@Override
	public List<Map<String, Object>> listarTodasTarefas() {
		return listarTarefas(null);
	}

	@Override
	public List<Map<String, Object>> listarTarefasPorDono(String dono) {
		List<Map<String, Object>> tarefas = new ArrayList<>();
		List<Task> tasks = taskService.createTaskQuery().taskAssignee(dono).list();
		for (Task task : tasks) {
			tarefas.add(makeTarefaDTO(task));
		}
		return tarefas;
	}

	@Override
	public List<Map<String, Object>> listarTarefasPorGrupo(String grupo) {
		List<Map<String, Object>> tarefas = new ArrayList<>();
		List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup(grupo).list();
		for (Task task : tasks) {
			tarefas.add(makeTarefaDTO(task));
		}
		return tarefas;
	}

	@Override
	public List<Map<String, Object>> listarTarefasPorAtividade(String atividade) {
		List<Map<String, Object>> tarefas = new ArrayList<>();
		List<Task> tasks = taskService.createTaskQuery().taskDefinitionKey(atividade).list();
		for (Task task : tasks) {
			tarefas.add(makeTarefaDTO(task));
		}
		return tarefas;
	}

	@Override
	public String alterarProcessoPorIdProcesso(Long idProcesso, Map<String, Object> processo, String usuario) throws TarefaNaoEncontradaException, TarefaNaoUnicaException, DadosInvalidosException {
		
		Map<String, Object> tarefa = obterTarefaPorIdProcesso(idProcesso);
		String taskId = (String) tarefa.get("idTarefa");
		String dono = (String) tarefa.get("dono");
		
		if (usuario == null || usuario.isEmpty()) {
			throw new UsuarioException("Usuário não informado");
		}else if (dono == null || dono.isEmpty()) {
			throw new UsuarioException("A tarefa precisa ser assumida antes de ser modificada");
		}else if (!usuario.toLowerCase().equals(dono.toLowerCase())) {
			throw new UsuarioException("Usuário não é dono do processo");
		}else if (processo == null || processo.get("idProcesso") == null) {
			throw new DadosInvalidosException("É necessário informar o código do processo");
		}
		
		LOGGER.debug("Alterando Processo: {}", JsontUtil.convertObjectToJsonString(processo));
		
		String executionId = taskService.createTaskQuery().taskId(taskId).singleResult().getExecutionId();
		//runtimeService.setsetVariable(executionId, "descricao", processo.get("descricao"));
		//runtimeService.setVariable(executionId, "aprovado", processo.get("aprovado"));
		runtimeService.setVariables(executionId, processo);
		return executionId;
	}


	@Override
	public Map<String, Object> obterTarefaPorIdProcesso(Long idProcesso) throws TarefaNaoEncontradaException, TarefaNaoUnicaException {
		List<Map<String, Object>> tasks = obterTarefasPorIdProcesso(idProcesso);
		
		if (tasks.size() == 0) {
			throw new TarefaNaoEncontradaException("Tarefa não encontrada");
		} else if (tasks.size() > 1) {
			throw new TarefaNaoUnicaException("Mais de uma tarefa encontrada para este processo");
		}
		
		Map<String, Object> tarefa = tasks.get(0);
		return tarefa;
	}
	
	

	@Override
	public void forcarTarefa(String tarefaAnterior, String novaTarefa, Map<String, Object> processo) {

		LOGGER.debug("Cancelando a tarefa {} e iniciando {}", tarefaAnterior, novaTarefa);
		
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().singleResult();
		runtimeService.createProcessInstanceModification(processInstance.getId())
		  .startBeforeActivity(novaTarefa)
		  .setVariables(processo)
		  .cancelAllForActivity(tarefaAnterior)
		  .execute();
		
	}
	
	

	@Override
	public List<Map<String, Object>> listarHistoricoProcesso() {

		List<Map<String, Object>> lista = new ArrayList<>();
		
		List<HistoricProcessInstance> historico = historyService.createHistoricProcessInstanceQuery()
		  .finished()
		  .orderByProcessInstanceDuration().desc()
		  .listPage(0, 10);
		
		for (HistoricProcessInstance h: historico) {
			System.out.println(h.getProcessDefinitionKey() + "\t" + h.getState());
			
			List<HistoricVariableInstance> histV = historyService.createHistoricVariableInstanceQuery()
			  .processInstanceId(h.getId())
			  .orderByVariableName().desc()
			  .list();
			for(HistoricVariableInstance v: histV) {
				System.out.println(v.getName() + ": " + v.getValue());
			}
			
			
			List<HistoricActivityInstance> histA = historyService.createHistoricActivityInstanceQuery()
			  .processDefinitionId(h.getProcessDefinitionId())
			  .finished()
			  .orderByHistoricActivityInstanceEndTime().desc()
			  .listPage(0, 10);
			
			for (HistoricActivityInstance a: histA) {
				System.out.println("\t"+a.getActivityName() + "\t" + a.getAssignee() + "\t" + a.getActivityType() + "\t"+ a.getDurationInMillis() + "\t"+a.getStartTime());
			}
			
			
		}
		
		return lista;
		
	}

	private List<Map<String, Object>> makeTarefasDTO(List<Task> tasks) {
		return tasks.stream().map(task -> makeTarefaDTO(task)).collect(Collectors.toList());
	}

	private Map<String, Object> makeTarefaDTO(Task task) {
		Map<String, Object> vars = runtimeService.getVariables(task.getExecutionId());

		//Inclui os dados da tarefa no mapa de variaveis
		vars.put("dono", task.getAssignee());
		vars.put("nomeAtividade", task.getName());
		vars.put("idTarefa", task.getId());
		vars.put("idAtividade", task.getTaskDefinitionKey());
				
		//Inclui os dados de candidatos no mapa de variaveis
		List<IdentityLink> links = taskService.getIdentityLinksForTask(task.getId());
		if (links != null && !links.isEmpty() && links.get(0).getGroupId() != null) {
			vars.put("grupoCandidato", links.get(0).getGroupId());
		}
		

		return vars;
	}

	
	private List<Map<String, Object>> obterTarefasPorIdProcesso(Long idProcesso) {
		List<Task> tasks = taskService.createTaskQuery().processVariableValueEquals("idProcesso", idProcesso).list();
		return makeTarefasDTO(tasks);
	}
	
	private Map<String, Object> obterTarefaPorIdTarefa(String idTarefa) {
		Task task = taskService.createTaskQuery().taskId(idTarefa).singleResult();
		return makeTarefaDTO(task);
	}
	
	private List<Map<String, Object>> listarTarefas(String taskDefinition) {
		List<Map<String, Object>> tarefas = new ArrayList<>();

		TaskQuery tq = taskService.createTaskQuery().active();
		if (taskDefinition != null && !taskDefinition.equals("all")) {
			tq = tq.taskDefinitionKey(taskDefinition);
		}
		List<Task> tasks = tq.list();

		for (Task task : tasks) {
			tarefas.add(makeTarefaDTO(task));
		}
		return tarefas;
	}

}
