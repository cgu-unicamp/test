package br.unicamp.cgu.camunda.exception;

public class UsuarioException extends TarefaNaoEncontradaException {
	
	private static final long serialVersionUID = 1L;
	
	public UsuarioException(String message) {
		super(message);
	}
	
	public UsuarioException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public UsuarioException(Throwable cause) {
		super(cause);
	}
	
}
