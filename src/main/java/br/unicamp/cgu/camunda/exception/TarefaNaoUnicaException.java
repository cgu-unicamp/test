package br.unicamp.cgu.camunda.exception;

public class TarefaNaoUnicaException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public TarefaNaoUnicaException(String message) {
		super(message);
	}
	
	public TarefaNaoUnicaException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public TarefaNaoUnicaException(Throwable cause) {
		super(cause);
	}
	
}

