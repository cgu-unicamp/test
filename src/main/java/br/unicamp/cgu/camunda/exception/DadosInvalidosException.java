package br.unicamp.cgu.camunda.exception;

public class DadosInvalidosException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public DadosInvalidosException(String message) {
		super(message);
	}
	
	public DadosInvalidosException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public DadosInvalidosException(Throwable cause) {
		super(cause);
	}
	
}

