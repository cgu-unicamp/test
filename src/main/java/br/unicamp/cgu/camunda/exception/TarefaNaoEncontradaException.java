package br.unicamp.cgu.camunda.exception;

public class TarefaNaoEncontradaException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public TarefaNaoEncontradaException(String message) {
		super(message);
	}
	
	public TarefaNaoEncontradaException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public TarefaNaoEncontradaException(Throwable cause) {
		super(cause);
	}
	
}

