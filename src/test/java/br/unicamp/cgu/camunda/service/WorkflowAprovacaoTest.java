package br.unicamp.cgu.camunda.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:testApplicationContext.xml"})
@WebAppConfiguration
@ActiveProfiles("production")
public class WorkflowAprovacaoTest {

	@Inject
	BpmService service;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowAprovacaoTest.class);


	@Test
	public void testSimpleWorkflowLifecycle() throws Exception {
		
		Long idProcesso = 1000L;
		
		String usuario1 = "Professor 001";
		String grupo1 = "docente";
		
		String usuario2 = "Diretor 001";
		String grupo2 = "comissao";
		
		Map<String, Object> convenio = new HashMap<>();
		convenio.put("tipo", 1);
		convenio.put("titulo", "Convenio Teste");
		convenio.put("descricao", "Apenas um convenio de teste");
		convenio.put("idProcesso", 1000L);
		convenio.put("aprovado", false);

		// 1 - Preencher Documento
		service.iniciarProcesso("aprovacao", convenio);
		service.assumirTarefaPorIdProcesso(idProcesso, usuario1, grupo1);
		service.completarTarefaPorIdProcesso(idProcesso, usuario1);
		
		Map<String, Object> tarefa = service.obterTarefaPorIdProcesso(idProcesso);
		LOGGER.debug("Proxima Tarefa: {} Grupo Candidato: {}", tarefa.get("idAtividade"), tarefa.get("grupoCandidato"));
		
		
		// 2 - Analisar / reprovar
		convenio.put("aprovado", false);
		service.assumirTarefaPorIdProcesso(idProcesso, usuario2, grupo2);
		service.alterarProcessoPorIdProcesso(idProcesso, convenio, usuario2);
		service.completarTarefaPorIdProcesso(idProcesso, usuario2);
		
		tarefa = service.obterTarefaPorIdProcesso(idProcesso);
		LOGGER.debug("Proxima Tarefa: {} Grupo Candidato: {}", tarefa.get("idAtividade"), tarefa.get("grupoCandidato"));

		// 3 - Alterar descricao
		convenio.put("descricao", "esta descricao foi alterada!!!");
		service.assumirTarefaPorIdProcesso(idProcesso, usuario1, grupo1);
		service.alterarProcessoPorIdProcesso(idProcesso, convenio, usuario1);
		service.completarTarefaPorIdProcesso(idProcesso, usuario1);
		
		tarefa = service.obterTarefaPorIdProcesso(idProcesso);
		LOGGER.debug("Proxima Tarefa: {} Grupo Candidato: {}", tarefa.get("idAtividade"), tarefa.get("grupoCandidato"));

		// 4 - Analisar / Aprovar
		convenio.put("aprovado", true);
		service.assumirTarefaPorIdProcesso(idProcesso, usuario2, grupo2);
		service.alterarProcessoPorIdProcesso(idProcesso, convenio, usuario2);
		service.completarTarefaPorIdProcesso(idProcesso, usuario2);

		
		// Listar
		List<Map<String, Object>> tasks = service.listarTodasTarefas();
		for(Map<String, Object> t: tasks) {
			System.out.println(t.get("idProcesso") + "\t" + t.get("titulo") + "\t" + t.get("idAtividade"));
		}
		List<Map<String, Object>> list = service.listarTarefasPorGrupo("comissao");
		for(Map<String, Object> t: list) {
			System.out.println(t.get("idProcesso") + "\t" + t.get("titulo") + "\t" + t.get("idAtividade"));
		}
		list = service.listarTarefasPorAtividade("analisar");
		for(Map<String, Object> t: list) {
			System.out.println(t.get("idProcesso") + "\t" + t.get("titulo") + "\t" + t.get("idAtividade"));
		}
		list = service.listarTarefasPorDono("Diretor 001");
		for(Map<String, Object> t: list) {
			System.out.println(t.get("idProcesso") + "\t" + t.get("titulo") + "\t" + t.get("idAtividade"));
		}

		
		//Listar Historico
		tasks = service.listarHistoricoProcesso();

		
	}
	
	
	@Test
	public void testForceTask() throws Exception {

		Long idProcesso = 1000L;
		
		String usuario1 = "Professor 001";
		String grupo1 = "docente";
		
		String usuario2 = "Diretor 001";
		String grupo2 = "comissao";
		
		Map<String, Object> convenio = new HashMap<>();
		convenio.put("tipo", 1);
		convenio.put("titulo", "Convenio Teste");
		convenio.put("descricao", "Apenas um convenio de teste");
		convenio.put("idProcesso", 1000L);
		convenio.put("aprovado", false);

		// 1 - Preencher Documento
		service.iniciarProcesso("aprovacao", convenio);
		service.assumirTarefaPorIdProcesso(idProcesso, usuario1, grupo1);
		service.completarTarefaPorIdProcesso(idProcesso, usuario1);
		
		Map<String, Object> tarefa = service.obterTarefaPorIdProcesso(idProcesso);
		LOGGER.debug("Proxima Tarefa: {} Grupo Candidato: {}", tarefa.get("idAtividade"), tarefa.get("grupoCandidato"));
		
		
		// 2 - Analisar / reprovar
		convenio.put("aprovado", false);
		service.assumirTarefaPorIdProcesso(idProcesso, usuario2, grupo2);
		service.alterarProcessoPorIdProcesso(idProcesso, convenio, usuario2);
		service.completarTarefaPorIdProcesso(idProcesso, usuario2);
		
		tarefa = service.obterTarefaPorIdProcesso(idProcesso);
		LOGGER.debug("Proxima Tarefa: {} Grupo Candidato: {}", tarefa.get("idAtividade"), tarefa.get("grupoCandidato"));

		// 3 - Forçar a aprovação mesmo tendo sido reprovado
		convenio.put("aprovado", true);
		service.forcarTarefa("alterar", "enviarEmail", convenio);
	}
	
	
	@Test
	public void testConvenio() throws Exception {
		
		Long idProcesso = 1000L;
		
		String usuario1 = "Professor 001";
		String grupo1 = "docente";

		Map<String, Object> convenio = new HashMap<>();
		convenio.put("tem_pi", true);
		convenio.put("minuta_padrao", true);
		convenio.put("interveniencia_funcamp", true);
		convenio.put("idProcesso", 1000L);
		convenio.put("analise_inova", false);
		convenio.put("analise_funcamp", false);
		convenio.put("analise_comissao", false);
		convenio.put("analise_congregacao", false);
		

		// 1 - Preencher Documento
		service.iniciarProcesso("convenio", convenio);
		service.assumirTarefaPorIdProcesso(idProcesso, usuario1, grupo1);
		service.completarTarefaPorIdProcesso(idProcesso, usuario1);
		
		Map<String, Object> tarefa = service.obterTarefaPorIdProcesso(idProcesso);
		LOGGER.debug("Proxima Tarefa: {} Grupo Candidato: {}", tarefa.get("idAtividade"), tarefa.get("grupoCandidato"));
		
		
	}
}
